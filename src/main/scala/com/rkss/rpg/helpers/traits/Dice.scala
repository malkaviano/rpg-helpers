package com.rkss.rpg.helpers.traits

trait Dice {
  def name: DiceName
  def roll: DiceResult
}
