package com.rkss.rpg.helpers.traits

trait DiceRange {
  def min: Int
  def max: Int
}
